#include <iostream>
#include <vector>

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
    const int SIZE = 100;
    int command, n;
	vector<vector<vector<int>>> a(SIZE);

    for (int i = 0; i < a.size(); i++) {
        a[i].resize(SIZE);
        for (int j = 0; j < a.size(); j++) {
            a[i][j].resize(SIZE);
            for (int k = 0; k < a.size(); k++) {
                a[i][j][k] = 0;
            }
        }
    }

    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> command;
        if (command == 1) {
            int x, y, z, value;
            cin >> x >> y >> z >> value;
            a[x - 1][y - 1][z - 1] = value;
        }
        else {
            int x0, y0, z0, x1, y1, z1, sum = 0;
            cin >> x0 >> y0 >> z0 >> x1 >> y1 >> z1;
            x0--; y0--; z0--; x1; y1; z1;
            for (int i = x0; i < x1; i++) {
                for (int j = y0; j < y1; j++) {
                    for (int k = z0; k < z1; k++) {
                        sum += a[i][j][k];
                    }
                }
            }
            cout << sum << '\n';
        }
    }
	return 0;
}