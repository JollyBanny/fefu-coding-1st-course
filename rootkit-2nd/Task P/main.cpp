#include <iostream>
#include <vector>

using namespace std;

int main()
{
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    int x;
    cin >> x;
    vector<int> ladders(x + 1);
    ladders[0] = 0;

    for(int i = 1; i <= x; ++i){
        cin >> ladders[i];
    }

    vector<int> dp(x+1);
    dp[0] = 0; dp[1] = ladders[1];

    for (int i = 2; i <= x; i++){
        dp[i] = max(dp[i-2], dp[i-1]) + ladders[i];
    }

    cout << dp[x];

    return 0;
}