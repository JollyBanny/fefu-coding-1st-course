#include <iostream>
#include <vector>

using namespace std;

int N;
char **maze;
const int dx[] = { 0, 1, 0, -1 };
const int dy[] = { -1, 0, 1, 0 };

struct Lost {
	int x;
	int y;
	int direction;
};
Lost first, second;

static int bad(int x, int y) {
	return x < 0 || x >= N || y < 0 || y >= N || maze[y][x] != '.';
}

int dfs() {
	int x1, y1, x2, y2, steps = 0, chance = 0;
	while (chance < 100) {
		x1 = first.x + dx[first.direction];
		y1 = first.y + dy[first.direction];
		x2 = second.x + dx[second.direction];
		y2 = second.y + dy[second.direction];

		if (bad(x1, y1)) {
			first.direction = (first.direction + 1) % 4;
		} else {
			first.x = x1;
			first.y = y1;
		}

		if (bad(x2, y2)) {
			second.direction = (second.direction - 1 + 4) % 4;
		} else {
			second.x = x2;
			second.y = y2;
		}
		steps++;
		if ((first.x == second.x && first.y == second.y) ||
			(first.x == second.x - dx[second.direction] &&
			 first.y == second.y - dy[second.direction] &&
			 second.x == first.x - dx[first.direction] &&
			 second.y == first.y - dy[first.direction])) {
			return steps;
		}
		chance++;
	}
	return -1;
}

int main() {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	cin >> N >> first.x >> first.y >> first.direction >> second.x >> second.y >>
		second.direction;
	first.x--;
	first.y--;
	first.direction--;
	second.x--;
	second.y--;
	second.direction--;
	maze = new char *[N];

	for (int i = 0; i < N; i++) {
		maze[i] = new char[N];
		string s;
		cin >> s;
		for (int j = 0; j < N; j++) {
			maze[i][j] = s[j];
		}
	}

	cout << dfs();
	return 0;
}