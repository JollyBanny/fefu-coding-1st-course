#include <iostream>
#include <vector>

using namespace std;

int main()
{
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    int S = 0, max = 0, count, x;
    cin >> count;
    for (int i = 0; i < count; i++){
        cin >> x;
        if (x > max) max = x;
        S += x;
    }
    S -= max;
    cout << S;

    return 0;
}