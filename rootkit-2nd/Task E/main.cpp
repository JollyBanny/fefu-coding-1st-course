#include <iostream>
#include <vector>

using namespace std;

void swap(vector<int> &a, int i, int j) {
	if (i != j) {
		int tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
	}
}

void quick_sort_r(vector<int> &a, int l, int r, int k) {
	if (l < r) {
		int pivot = a[(l + r) / 2];
		int i = l, j = r;
		while (i <= j) {
			while (a[i] < pivot)
				i++;
			while (a[j] > pivot)
				j--;
			if (i >= j) {
				break;
			}
			swap(a, i, j);
			i++;
			j--;
		}
		if (j < k) {
			quick_sort_r(a, j + 1, r, k);
		} else {
			quick_sort_r(a, l, j, k);
		}
	}
}

void quick_sort(vector<int> &a, int k) { quick_sort_r(a, 0, a.size() - 1, k); }

int main() {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	int n, k;
	cin >> n >> k;
	vector<int> mas(n);

	for (int i = 0; i < mas.size(); i++) {
		cin >> mas[i];
	}

	quick_sort(mas, k - 1);
	cout << mas[k - 1];

	return 0;
}