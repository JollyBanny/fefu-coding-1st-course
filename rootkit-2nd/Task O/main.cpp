#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void bubble_sort(vector<int> &a) {
	for(size_t i = 0; i < a.size(); i++){
		for(size_t j = 0; j < a.size(); j++){
			if(a[i] < a[j]) {
				swap(a[i], a[j]);
			}
		}
	}
}

int main()
{
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    int x;
    cin >> x;

    vector<int> nails(x+1);
    vector<int> dp(x+1);

    for(int i = 1; i <= x; ++i){
        cin >> nails[i];
    }
    bubble_sort(nails);

    dp[0] = 0;
    dp[1] = nails[2];

    for (int i = 2; i <= x; ++i){
        dp[i] = min(dp[i-2], dp[i-1]) + nails[i] - nails[i-1];
    }
    cout << dp[x];

    return 0;
}