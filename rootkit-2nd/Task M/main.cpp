#include <iostream>
#include <string>

using namespace std;

int N, M;
char **maze;
const int dx[] = { -1, 1, 0, 0 };
const int dy[] = { 0, 0, -1, 1 };

void print() {
	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < M; ++j) {
			cout << maze[i][j];
		}
		if (i != N - 1)
			cout << "\n";
	}
}

void destruct() {
	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < M; ++j) {
			if (maze[i][j] == '-')
				maze[i][j] = '.';
		}
	}
}

static int bad(int x, int y) { return x < 0 || x >= M || y < 0 || y >= N; }

void depth_first_search(int x, int y) {
		if (maze[y][x] == '.')
			return;
		for (int i = 0; i < 4; ++i) {
			int xx = x + dx[i], yy = y + dy[i];
			if (bad(xx, yy))
				continue;
			if (maze[yy][xx] == '.')
				return;
		}
		maze[y][x] = '-';
}

int main() {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	cin >> N >> M;
	maze = new char *[N];

	for (int i = 0; i < N; ++i) {
		maze[i] = new char[M];
		string s;
		cin >> s;
		for (int j = 0; j < M; ++j) {
			maze[i][j] = s[j];
		}
	}

	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < M; ++j) {
			depth_first_search(j, i);
		}
	}

	destruct();
	print();
	return 0;
}