#include <iostream>
#include <string>
#include <vector>

using namespace std;

void print(vector<string> &a){
    for (size_t i = 0; i < a.size(); i++){
        cout << a[i];
        if (i != a.size() - 1) cout << "\n";
    }
}

void pl_sort (vector<string> &a){
	for(size_t i = 0; i < a.size(); i++){
		for(size_t j = 0; j < a.size(); j++){
			if(a[i] < a[j]) {
				swap(a[i], a[j]);
			}
		}
	}
}

string string_search(string &s){
    int index = s.size();
    for(int i = 0; i < index; i++){
        if(s[i] == ' ' || s[i] == '_' || s[i] == '-') {
            index = i;
            break;
        }
    }
    return s.substr(0, index);
}

int main()
{
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    int x;
    vector<string> pl1, pl2, finpl, temp_pl;
    cin >> x;
    pl1.resize(x);
    getline(cin, pl1[0]);
    for (int i = 0; i < x; i++){
        getline(cin, pl1[i]);
    }

    cin >> x;
    pl2.resize(x);
    if (x != 0) getline(cin, pl2[0]);
    for (int i = 0; i < x; i++){
        getline(cin, pl2[i]);
    }

    for(size_t i = 0; i < pl2.size(); i++){
        for(size_t j = 0; j < pl1.size(); j++){
            if (string_search(pl2[i]) == string_search(pl1[j])){
                temp_pl.push_back(pl1[j]);
            }
        }
        pl_sort(temp_pl);
        for (size_t i = 0; i < temp_pl.size(); i++){
            finpl.push_back(temp_pl[i]);
        }
        temp_pl.clear();
    }
    print(finpl);
    return 0;
}