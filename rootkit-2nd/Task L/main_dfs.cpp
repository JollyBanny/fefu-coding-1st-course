#include <iostream>
#include <vector>

using namespace std;

int N;
char **maze;
int best;
const int dx[] = { -1, 1, 0, 0 };
const int dy[] = { 0, 0, -1, 1 };

static int min1(int a, int b) {
    return
        a == -1 ? b :
        b == -1 ? a :
        a > b ? b :
        a;
}

static int bad(int x, int y) {
    return x < 0 || x >= N || y < 0 || y >= N || maze[y][x] != '.';
}

void depth_first_search(int x0, int y0, int len, int x1, int y1) {
    if (x0 == x1 - 1 && y0 == y1 - 1) {
        best = min1(len, best);
        return;
    }
    if (best != -1 && (2 * N - 2 - x0 - y0 + len) >= best){
        return;
    }
    maze[y0][x0] = '*';
    for(int i = 0; i < 4; i++) {
        int xx = x0 + dx[i], yy = y0 + dy[i];
        if (bad(xx, yy))
            continue;
        depth_first_search(x0 + dx[i], y0 + dy[i], len + 1, x1, y1);
    }
    maze[y0][x0] = '.';
    return;
}

int main() {
  freopen("input.txt", "r", stdin);
  freopen("output.txt", "w", stdout);

  int x0, y0, x1, y1;
  cin >> N >> x0 >> y0 >> x1 >> y1;
  x0--; y0--;
  maze = new char*[N];

  for (int i = 0; i < N; i++) {
	maze[i] = new char[N];;
	string s;
	cin >> s;
	for (int j = 0; j < N; j++) {
	  maze[i][j] = s[j];
	}
  }
  best = -1;
  depth_first_search(x0, y0, 0, x1, y1);
  cout << best;
  return 0;
}