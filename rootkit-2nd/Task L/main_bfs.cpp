#include <algorithm>
#include <iostream>

using namespace std;

int N;
int **maze_bool;
char **maze;
const int dx[] = { -1, 1, 0, 0 };
const int dy[] = { 0, 0, -1, 1 };

struct Point {
	int x, y;
};
struct Queue {
	Point *q;
	int head;
	int tail;
};

void push(Queue &queue, Point value) { queue.q[queue.tail++] = value; }

Point pop(Queue &queue) { return queue.q[queue.head++]; }

bool empty(Queue &queue) { return queue.head == queue.tail; }

static int bad(int x, int y) {
	return x < 0 || x >= N || y < 0 || y >= N || maze[y][x] != '.' ||
		   maze_bool[y][x] != -1;
}

int breadth_first_search(int x0, int y0, int x1, int y1) {
	Queue q = { new Point[N * N], 0, 0 };
	push(q, Point{ x0, y0 });
	maze_bool[y0][x0] = 0;
	while (!empty(q)) {
		Point p = pop(q);
		if (p.x == x1 - 1 && p.y == y1 - 1) {
			return maze_bool[p.y][p.x];
		}
		for (int i = 0; i < 4; i++) {
			int xx = p.x + dx[i], yy = p.y + dy[i];
			if (bad(xx, yy))
				continue;
			maze_bool[yy][xx] = maze_bool[p.y][p.x] + 1;
			push(q, Point{ xx, yy });
		}
	}
	return -1;
}

int main() {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	int x0, y0, x1, y1;
	cin >> N >> x0 >> y0 >> x1 >> y1;
	x0--;
	y0--;
	maze = new char *[N];
	maze_bool = new int *[N];

	for (int i = 0; i < N; i++) {
		maze[i] = new char[N];
		;
		maze_bool[i] = new int[N];
		fill(maze_bool[i], maze_bool[i] + N, -1);
		string s;
		cin >> s;
		for (int j = 0; j < N; j++) {
			maze[i][j] = s[j];
		}
	}

	cout << breadth_first_search(x0, y0, x1, y1);
	return 0;
}