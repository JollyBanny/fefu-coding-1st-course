#include <iostream>
#include <string>
#include <vector>

using namespace std;

void prefix_function(vector<int> &pi, string substr) {
	pi[0] = 0;
	int j = 0, i = 1;
	while (i < substr.length()) {
		if (substr[i] == substr[j]) {
			pi[i] = j + 1;
			i++;
			j++;
		} else if (j == 0) {
			pi[i] = 0;
			i++;
		} else {
			j = pi[j - 1];
		}
	}
}

int KMP_function(string str, string substr, vector<int> &pi) {
	int k = 0, l = 0;
	while (k < str.length()) {
		if (l == substr.length()) {
			return k - substr.length() + 1;
		}
		if (substr[l] == str[k]) {
			k++;
			l++;
		} else {
			if (l != 0)
				l = pi[l - 1];
			else
				k++;
		}
	}
	if (l == substr.length()) {
		return k - substr.length() + 1;
	}
	return -1;
}

int main() {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	string str;
	string substr;
	getline(cin, str);
	getline(cin, substr);

	vector<int> pi(str.length());

	prefix_function(pi, substr);
	cout << KMP_function(str, substr, pi);

	return 0;
}