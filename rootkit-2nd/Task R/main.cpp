#include <iostream>
#include <vector>

#define SIZE 1000000

using namespace std;

struct Map_Open_Address {
	vector<int> table;
	int size = 0;
};

void init(Map_Open_Address &map) {
	map.table.resize(SIZE);
	for (int i = 0; i < 1e6; i++) {
		map.table[i] = -1;
	}
}

int function_hash(int key) { return key % SIZE; }

void add(Map_Open_Address &map, int key) {
	int i = function_hash(key);
	while (true) {
		if (map.table[i] == key) {
			return;
		}
		if (map.table[i] == -1 || map.table[i] == -2) {
			map.table[i] = key;
			return;
		}
		i = (i + 1) % SIZE;
	}
}

void del(Map_Open_Address &map, int key) {
	key = abs(key);
	int i = function_hash(key);
	while (true) {
		if (map.table[i] == key) {
			map.table[i] = -2;
			return;
		}
		if (map.table[i] == -1) {
			return;
		}
		i = (i + 1) % SIZE;
	}
}

void swap(vector<int> &a, int i, int j) {
	if (i != j) {
		int tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
	}
}

void quick_sort(vector<int> &a, int l, int r) {
	if (l < r) {
		int pivot = a[(l + r) / 2];
		int i = l, j = r;
		while (i <= j) {
			while (a[i] < pivot)
				i++;
			while (a[j] > pivot)
				j--;
			if (i >= j) {
				break;
			}
			swap(a, i, j);
			i++;
			j--;
		}
		quick_sort(a, j + 1, r);
		quick_sort(a, l, j);
	}
}

void print(vector<int> &a) {
	for (size_t i = 0; i < a.size(); i++) {
		cout << a[i];
		if (i != a.size() - 1)
			cout << " ";
	}
}

int main() {
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	int key;
	vector<int> fin;
	Map_Open_Address map;
	init(map);

	cin >> key;
	while (key != 0) {
		if (key > 0)
			add(map, key);
		else
			del(map, key);
		cin >> key;
	}
	for (int i = 0; i < SIZE; i++) {
		if (map.table[i] != -1 && map.table[i] != -2)
			fin.push_back(map.table[i]);
	}

	quick_sort(fin, 0, fin.size() - 1);
	print(fin);
}