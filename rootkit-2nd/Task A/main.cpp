#include <iostream>
#include <vector>

using namespace std;

void bubble_sort (vector<int> &a, vector<int> &b){
	for(int i = 0; i < a.size(); i++){
		for(int j = 0; j < a.size(); j++){
			if(a[i] > a[j]) {
				swap(a[i], a[j]);
				swap(b[i], b[j]);
			}
		}
	}
}

void print(vector<int> &a){
	for(int i = 0; i < a.size(); i++){
		cout << a[i] << " ";
	}
}

int main()
{
    freopen("c.in", "r", stdin);
    freopen("c.out", "w", stdout);

    vector<int> Tasks, Index;
    int a;
	cin >> a;

    for (int i = 0; i < a; ++i){
        int x;
        cin >> x;
        Tasks.push_back(x);
		Index.push_back(i + 1);
    }
    bubble_sort(Tasks, Index);

	print(Index);

    return 0;
}