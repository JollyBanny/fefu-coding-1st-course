#include <iostream>
#include <string>
#include <vector>

using namespace std;

vector<string> combinations(string &a) {
  vector<string> result;

  for (int i = 0; i < a.size(); i++) {
	for (int j = 0; j < a.size(); j++) {
	  for (int k = 0; k < a.size(); k++) {
		if (i != j && j != k && i != k) {
		  string tmp;
		  tmp.push_back(a[i]);
		  tmp.push_back(a[j]);
		  tmp.push_back(a[k]);

		  bool flag = true;
		  for (size_t l = 0; l < result.size(); l++) {
			if (result[l] == tmp)
			  flag = false;
		  }
		  if (flag)
			result.push_back(tmp);
		}
	  }
	}
  }
  return result;
}

void all_combination(vector<string> &a, vector<string> &b) {
  cout << a.size() * b.size();
  for (int i = 0; i < a.size(); i++) {
	for (int j = 0; j < b.size(); j++) {
	  cout << '\n'
		   << a[i][0] << b[j][0] << b[j][1] << b[j][2] << a[i][1] << a[i][2];
	}
  }
}

int main() {
  freopen("numbers.in", "r", stdin);
  freopen("numbers.out", "w", stdout);

  const int x = 3;
  string car_number, car_abc;
  car_number.resize(x);
  car_abc.resize(x);
  vector<char> found_numbers;
  cin >> car_abc[0] >> car_number[0] >> car_number[1] >> car_number[2] >>
	  car_abc[1] >> car_abc[2];
  vector<string> car_number_vec = combinations(car_number);
  vector<string> car_abc_vec = combinations(car_abc);

  all_combination(car_abc_vec, car_number_vec);

  return 0;
}