#include <iostream>
#include <string>
#include <vector>

using namespace std;

struct Soldiers
{
    float height;
    string name;
};


int merge(vector<Soldiers> &a, vector<Soldiers> &b, int left, int mid, int right){
    long long int result = 0;
    int i = 0, j = 1;
    while (left + i <= mid && mid + j <= right){
        if(a[left + i].height >= a[mid + j].height){
            b[left + i + j - 1] = a[left + i];
            i++;
        }
        else {
            b[left + i + j -1] = a[mid + j];
            result += mid - (left + i) + 1;
            j++;
        }
    }
    while(left + i <= mid){
        b[left + i + j - 1] = a[left + i];
        i++;
    }
    while(mid + j <= right){
        b[left + i + j - 1] = a[mid + j];
        j++;
    }

    for (int k = 0; k < i + j - 1; k++){
        a[left + k] = b[left + k];
    }
    return result;
}

long long int merge_sort(vector<Soldiers> &a, vector<Soldiers> &b, int left, int right){
    if (left >= right) return 0;
    long long int result = 0;
    int middle = (left + right) / 2;
    result += merge_sort(a, b, left, middle);
    result += merge_sort(a, b, middle + 1, right);
    result += merge(a, b, left, middle, right);
    return result;
}

void print(vector<Soldiers> &a){
    for (size_t i = 0; i < a.size(); i++){
        cout << a[i].name;
        if (i != a.size() - 1) cout << "\n";
    }
}

int main()
{
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    int x;
    vector<Soldiers> army, temp;
    cin >> x;
    army.resize(x);
    temp.resize(x);

    for (int i = 0; i < x; i++){
        cin >> army[i].height >> army[i].name;
    }
    cout << merge_sort(army, temp, 0, x - 1);
    return 0;
}