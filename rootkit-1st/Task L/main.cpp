#include <iostream>

using namespace std;

int make_mask(int x) { return ((1 << 8) - 1) << x; }

int ch(int &bits, char &rgb) {
  int r, g, b;
  int color = 0;
  int mask = 0;
  switch (rgb) {
  case 'r': {
	mask = make_mask(16);
	break;
  }
  case 'g': {
	mask = make_mask(8);
	break;
  }
  case 'b': {
	mask = make_mask(0);
	break;
  }
  default:
	break;
  }

  return (bits & mask);
}

int main() {
  freopen("input.txt", "r", stdin);
  freopen("output.txt", "w", stdout);

  int bits;
  char rgb;
  cin >> bits >> rgb;

  cout << ch(bits, rgb);

  return 0;
}