#include <iostream>

using namespace std;

unsigned long long int frame(unsigned long long int w, unsigned long long int h,
							 unsigned long long int n) {
  unsigned long long int left = 0;
  unsigned long long int right = 1e18;

  while (left < right) {
	unsigned long long int middle = (left + right) / 2;

	unsigned long long int width_count = middle / w;
	unsigned long long int height_count = middle / h;

	if (n <= width_count * height_count) {
	  right = middle;
	} else {
	  left = middle + 1;
	}
  }

  return left;
}

int main() {
  freopen("diploma.in", "r", stdin);
  freopen("diploma.out", "w", stdout);

  unsigned long long int W, H, N;
  cin >> W >> H >> N;

  cout << frame(W, H, N);

  return 0;
}