#include <fstream>
#include <iostream>

using namespace std;

int main() {
  ifstream in("input.txt");
  ofstream out("output.txt");

  int A[1000], i, N, s = 0;
  in >> N;
  cout << N << endl;
  for (i = 0; i < N; i++) {
	in >> A[i];
	cout << A[i];
  }
  in.close();

  for (i = 0; i < N; i++) {
	s += A[i];
	cout << s << endl;
  }
  out << s << endl;
  out.close();

  return 0;
}