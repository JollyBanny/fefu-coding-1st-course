#include "linear_sequence.h"
#include <iostream>

struct Container {
	LSQ_BaseTypeT *data;
	LSQ_IntegerIndexT size;
	LSQ_IntegerIndexT capacity;
};

struct Iterator {
	Container *container;
	LSQ_IntegerIndexT index;
};

extern LSQ_HandleT LSQ_CreateSequence(void) {
	Container *container = (Container *)malloc(sizeof(Container));
	container->capacity = 10;
	container->size = 0;
	container->data = (LSQ_BaseTypeT *)malloc(sizeof(LSQ_BaseTypeT) * container->capacity);

	return container;
}

extern void LSQ_DestroySequence(LSQ_HandleT handle) {
	Container *container = (Container *)handle;
	container->size = 0;
	container->capacity = 0;
	free(container->data);
	free(container);
}

extern LSQ_IntegerIndexT LSQ_GetSize(LSQ_HandleT handle) {
	Container *container = (Container *)handle;
	return container->size;
}

extern int LSQ_IsIteratorDereferencable(LSQ_IteratorT iterator) {
	Iterator *iter = (Iterator *)iterator;
	return (iter->index < iter->container->size &&
			iter->index >= 0);
}

extern int LSQ_IsIteratorPastRear(LSQ_IteratorT iterator) {
	Iterator *iter = (Iterator *)iterator;
	return (iter->index == iter->container->size);
}

extern int LSQ_IsIteratorBeforeFirst(LSQ_IteratorT iterator) {
	Iterator *iter = (Iterator *)iterator;
	return (iter->index == -1);
}

extern LSQ_BaseTypeT *LSQ_DereferenceIterator(LSQ_IteratorT iterator) {
	Iterator *iter = (Iterator *)iterator;
	return iter->container->data + iter->index;
}

/* init Iterator, this get handle and index */
LSQ_IteratorT Create_Iterator(LSQ_HandleT handle, LSQ_IntegerIndexT index) {
	Iterator *iter = (Iterator *)malloc(sizeof(Iterator));
	iter->container = (Container *)handle;
	iter->index = index;
	return iter;
}

extern LSQ_IteratorT LSQ_GetElementByIndex(LSQ_HandleT handle,
										   LSQ_IntegerIndexT index) {
	return Create_Iterator(handle, index);
}

extern LSQ_IteratorT LSQ_GetFrontElement(LSQ_HandleT handle) {
	return Create_Iterator(handle, 0);
}

extern LSQ_IteratorT LSQ_GetPastRearElement(LSQ_HandleT handle) {
	return Create_Iterator(handle, ((Container *)handle)->size);
}

extern void LSQ_DestroyIterator(LSQ_IteratorT iterator) {
	Iterator *iter = (Iterator *)iterator;
	free(iter);
}

extern void LSQ_AdvanceOneElement(LSQ_IteratorT iterator) {
	/* refer to function LSQ_ShiftPosition with increment */
	LSQ_ShiftPosition(iterator, 1);
}

extern void LSQ_RewindOneElement(LSQ_IteratorT iterator) {
	/* refer to function LSQ_ShiftPosition with decrement */
	LSQ_ShiftPosition(iterator, -1);
}

extern void LSQ_ShiftPosition(LSQ_IteratorT iterator, LSQ_IntegerIndexT shift) {
	Iterator *iter = (Iterator *)iterator;
	iter->index += shift;
}

extern void LSQ_SetPosition(LSQ_IteratorT iterator, LSQ_IntegerIndexT pos) {
	Iterator *iter = (Iterator *)iterator;
	iter->index = pos;
}

extern void LSQ_InsertFrontElement(LSQ_HandleT handle, LSQ_BaseTypeT element) {
	/* create iter with first index */
	LSQ_IteratorT iter = Create_Iterator(handle, 0);
	LSQ_InsertElementBeforeGiven(iter, element);
	LSQ_DestroyIterator(iter);
}

extern void LSQ_InsertRearElement(LSQ_HandleT handle, LSQ_BaseTypeT element) {
	/* create iter with last index */
	LSQ_IteratorT iter = Create_Iterator(handle, ((Container *)handle)->size);
	LSQ_InsertElementBeforeGiven(iter, element);
	LSQ_DestroyIterator(iter);
}

extern void LSQ_InsertElementBeforeGiven(LSQ_IteratorT iterator,
										 LSQ_BaseTypeT newElement) {
	Iterator *iter = (Iterator *)iterator;
	Container *cont = (Container*)iter->container;

	if (cont->capacity <= cont->size + 1) {
		cont->capacity *= 2;
		cont->data = (LSQ_BaseTypeT *)realloc(
			cont->data, (sizeof(LSQ_BaseTypeT)) * cont->capacity);
	}

	for (int i = cont->size; i > iter->index; --i) {
		cont->data[i] = cont->data[i - 1];
	}
	cont->data[iter->index] = newElement;
	cont->size++;
}

extern void LSQ_DeleteFrontElement(LSQ_HandleT handle) {
	LSQ_IteratorT iter = Create_Iterator(handle, 0);
	LSQ_DeleteGivenElement(iter);
	LSQ_DestroyIterator(iter);
}

extern void LSQ_DeleteRearElement(LSQ_HandleT handle) {
	LSQ_IteratorT iter = Create_Iterator(handle, ((Container *)handle)->size);
	LSQ_DeleteGivenElement(iter);
	LSQ_DestroyIterator(iter);
}

extern void LSQ_DeleteGivenElement(LSQ_IteratorT iterator) {
	Iterator *iter = (Iterator *)iterator;
	Container *cont = (Container*)iter->container;

	for (int i = iter->index; i < cont->size; ++i) {
		cont->data[i] = cont->data[i + 1];
	}
	cont->size--;
}