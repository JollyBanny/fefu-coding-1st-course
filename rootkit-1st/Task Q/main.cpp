#include"linear_sequence.h"

#include <iostream>
#include <windows.h>
#include <psapi.h>
#include <stdio.h>
#include <cassert>

typedef struct LSQ_Handle {
    LSQ_IntegerIndexT capacity;
    LSQ_IntegerIndexT size;
    LSQ_BaseTypeT* data;
} LSQ_Handle;

typedef struct LSQ_Iterator {
    LSQ_Handle* handle;
    LSQ_IntegerIndexT index;
} LSQ_Iterator;

int main1() {
    LSQ_HandleT handle = LSQ_CreateSequence();
    int n = 10;
    for (int i = 0; i < n; ++i) {
        LSQ_InsertFrontElement(handle, i * i);
    }
    return 0;
}


int main()
{
    LSQ_HandleT handle;
    LSQ_IteratorT it;
    for(int i = 0; i < 1000000; ++i) {
        handle = LSQ_CreateSequence();
        LSQ_InsertRearElement(handle, 0);
        LSQ_InsertRearElement(handle, 0);
        LSQ_InsertFrontElement(handle, 0);
        LSQ_InsertFrontElement(handle, 0);
        it = LSQ_GetElementByIndex(handle, 1);
        LSQ_InsertElementBeforeGiven(it, 0);
        LSQ_DestroyIterator(it);
        it = LSQ_GetFrontElement(handle);
        LSQ_AdvanceOneElement(it);
        LSQ_AdvanceOneElement(it);
        LSQ_AdvanceOneElement(it);
        LSQ_DestroyIterator(it);
        LSQ_DestroySequence(handle);
    }
    return 0;
}