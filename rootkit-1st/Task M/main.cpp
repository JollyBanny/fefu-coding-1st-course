#include <iostream>

using namespace std;

int bits(int &x) {
  int count = 0;
  while (x > 0) {
	count += x % 2;
	x = x / 2;
  }
  return count;
}

int main() {
  freopen("input.txt", "r", stdin);
  freopen("output.txt", "w", stdout);

  int x;
  cin >> x;

  cout << bits(x);

  return 0;
}