#include <iostream>
#include <math.h>

using namespace std;

struct Point {
  float x, y;
};

int main()

{
  Point P;
  cin >> P.x >> P.y;

  if (fabs(P.x) < 1e-16 | fabs(P.y) < 1e-16) {
	cout << 0;
  }

  else if (P.x > 0 && P.y > 0) {
	cout << 1;
  } else if (P.x < 0 && P.y > 0) {
	cout << 2;
  } else if (P.x < 0 && P.y < 0) {
	cout << 3;
  } else if (P.x > 0 && P.y < 0) {
	cout << 4;
  } else
	cout << 0;

  return 0;
}