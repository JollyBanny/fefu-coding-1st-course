#include <iostream>

using namespace std;

int main() {
  freopen("input.txt", "r", stdin);
  freopen("output.txt", "w", stdout);

  int k, L, d, c;
  cin >> L >> d >> c;

  if (L % d == 0) {
	cout << "0" << endl;
	return 0;
  }

  for (int i = 0; i <= L / d + 1; ++i) {
	if (d * (L / d + 1 - i) + (i * c) <= L) {
	  cout << i << endl;
	  return 0;
	}
  }
  cout << "-1" << endl;

  return 0;
}