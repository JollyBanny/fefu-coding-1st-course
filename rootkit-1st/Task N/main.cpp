#include <iostream>
#include <vector>

using namespace std;

int int_hex(int x) {
    if (x <= 9 && 0 <= x) return '0' + x;
    return x + 'A' - 10;
}

inline void print_hexs(vector<int> &hexs_int) {
    string str_hexs;
    for (int i = 0; i < hexs_int.size(); ++i) {
        int hex_int = hexs_int[i];
        if (hex_int < 0) {
            hex_int += 256;
        }
        str_hexs.push_back(int_hex(hex_int >> 4));
        str_hexs.push_back(int_hex(hex_int & ((1 << 4) - 1)));
    }
    cout << str_hexs;
}


inline void Feistel(int *left, int *right, int block_size, int rounds, int *key_value) {
    int q = 0;
    for (int r = 0; r < rounds; ++r) {
        for (int i = 0; i < block_size/2; ++i) {
            int temp = left[i];
            left[i] = right[i];
            right[i] = (temp ^ (key_value[q] ^ right[i]));
            ++q;
        }
    }
    vector<int> left_hexs;
    vector<int> right_hexs;
    for (int i = 0; i < block_size/2; ++i) {
        left_hexs.push_back(left[i]);
        right_hexs.push_back(right[i]);
    }
    print_hexs(left_hexs);
    print_hexs(right_hexs);
}

inline int hex_char(char hexch) {
    int intval = (hexch >= 'A') ? (hexch - 'A' + 10) : (hexch - '0');
    return intval;
}

int main() {
    ios_base::sync_with_stdio(false);
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    int n; cin >> n;


    for (int test = 0; test < n; ++test) {
        int rounds, block_size;
        string key, str;
        cin >> rounds >> block_size >> key >> str;

        while (str.length() % block_size != 0) {
            str += '\0';
        }

        // берем ключ
        int *key_value = new int[key.length() / 2];
        int m = 0;
        for (int i = 0; i < key.length(); i += 2) {
            int x = hex_char(key[i]), y = hex_char(key[i + 1]);
            int result = (x << 4) | y;
            key_value[m] = result;
            m++;
        }

        for(int i = 0; i < str.length(); i += block_size) {
            int *left = new int[block_size/2];
            int *right = new int[block_size/2];
            int j = 0, r = 0;

            while (j < block_size/2) {
                left[j] = ((int) (str[j + i]));
                ++j;
            }
            while(r < block_size/2) {
                right[r] = ((int) (str[j + i]));
                ++r;
                ++j;
            }
            Feistel(left, right, block_size, rounds, key_value);
        }
        cout << '\n' ;
    }
    return 0;
}