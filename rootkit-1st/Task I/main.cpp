#include <fstream>
#include <vector>
#include <iostream>

using namespace std;

int main() {
  ifstream in("input.txt");
  ofstream out("output.txt");

  int i, j, n, c = 0, s = -1;
  in >> n;
  vector<int> N(n);
  for (i = 0; i < n; i++) {
	in >> N[i];
  }
  in.close();
  for (i = 0; i < n; i++) {
	c = 0;
	for (j = 0; j < n; j++) {
	  if (N[j] % N[i] == 0) {
		c += 1;
	  } else
		break;
	}
	if (c == n) {
	  s = N[i];
	}
  }
  out << s;
  out.close();

  return 0;
}