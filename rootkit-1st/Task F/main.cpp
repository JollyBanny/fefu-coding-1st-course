#include <fstream>
#include <iostream>

using namespace std;

int main() {
  ifstream in("input.txt");
  ofstream out("output.txt");

  int A, B, S = 0;
  in >> A >> B;
  in.close();
  if ((A + B - 1) % 4 == 0) {
	if (A % 2 != B % 2) {
	  S = A + B - 1;
	}
  }
  out << S;
  out.close();

  return 0;
}