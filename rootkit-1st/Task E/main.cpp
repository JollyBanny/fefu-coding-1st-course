#include <fstream>
#include <iostream>

using namespace std;

int main() {
  ifstream in("input.txt");
  int A, B;
  in >> A >> B;
  in.close();

  ofstream out("output.txt");
  out << A + B;
  out.close();

  return 0;
}