#include <iostream>
#include <vector>

using namespace std;

enum State {
	INF = 1000 * 1000,
	NONE = -1,
};

vector<vector<int>> graph;

vector<int> dijkstra(int s) {
	vector<bool> visited;
	vector<int> d;
	visited.resize(graph.size(), false);
	d.resize(graph.size(), INF);
	d[s] = 0;
	for (int i = 0; i < graph.size(); ++i) {
		int nearest = NONE;

		for (int j = 0; j < graph.size(); ++j) {
			if (!visited[j] && (nearest == NONE || d[j] < d[nearest])) {
				nearest = j;
			}
		}
		if (d[nearest] == INF)
			break;

		visited[nearest] = true;

		for (int j = 0; j < graph[nearest].size(); ++j) {
			if (d[graph[nearest][j]] > d[nearest] + 1) {
				d[graph[nearest][j]] = d[nearest] + 1;
			}
		}
	}
	return d;
}

void init_line(vector<int> &line) {
	for (int i = 0; i < line.size(); ++i) {
		for (int j = 0; j < line.size(); ++j) {
			if (j != i)
				graph[line[i]].push_back(line[j]);
		}
	}
}

int main() {
	ios_base::sync_with_stdio(false);
	cin.tie(nullptr);
	cout.tie(nullptr);

	freopen("e.in", "r", stdin);
	freopen("e.out", "w", stdout);
	int n, m, from, to;
	cin >> n >> m;
	graph.resize(n);

	for (int i = 0; i < m; ++i) {
		int cnt_st;
		vector<int> line;
		cin >> cnt_st;
		for (int j = 0; j < cnt_st; ++j) {
			int station;
			cin >> station;
			line.push_back(--station);
		}
		init_line(line);
	}

	cin >> from >> to;
	--from, --to;
	vector<int> dist = dijkstra(from);
	if (dist[to] == INF)
		cout << -1;
	else
		cout << dist[to] - 1;
	return 0;
}