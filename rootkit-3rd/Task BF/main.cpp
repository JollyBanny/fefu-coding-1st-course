#include <iostream>
#include <vector>

using namespace std;
const int INF = 1000000000;

struct V {
	int from, to, len;
};
vector<V> G;
int n, m, s;

void print(vector<int> a) {
	for (int i = 0; i < a.size(); ++i) {
		if (a[i] >= INF) {
			cout << ' ';
		} else {
			cout << a[i];
		}
		cout << " ";
	}
}

vector<int> FB() {
	vector<int> d(n, INF);
	d[s] = 0;
	for (int i = 0; i < n - 1; ++i)
		for (int j = 0; j < m; ++j)
			if (d[G[j].from] < INF)
				d[G[j].to] = min(d[G[j].to], d[G[j].from] + G[j].len);
	return d;
}

int main() {
	ios_base::sync_with_stdio(false);
	cin.tie(nullptr);
	// freopen("input.txt", "r", stdin);
	// freopen("output.txt", "w", stdout);
	cin >> n >> m >> s;
	--s;
	for (int i = 0; i < m; ++i) {
		int u, v, w;
		cin >> u >> v >> w;
		--u, --v;
		G.push_back(V{u, v, w });
	}
	print(FB());
	return 0;
}