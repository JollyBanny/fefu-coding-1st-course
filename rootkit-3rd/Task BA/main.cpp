#include <iostream>
#include <vector>

using namespace std;

int n, m, s;
vector<vector<int>> graph;
vector<int> order;
char *used;

enum State {
	NONE,
	CAME,
	EXIT,
};

void explore(int v) {
	used[v] = CAME;
	for (int i = 0; i < graph[v].size(); ++i) {
		if (!used[graph[v][i]]) {
			explore(graph[v][i]);
		}
		if (used[graph[v][i]] == CAME) {
			cout << -1;
			delete[] used;
			exit(0);
		}
	}
	order.push_back(v);
	used[v] = EXIT;
}

void dfs() {
	for (int i = 0; i < n; ++i) {
		if (!used[i]) {
			explore(i);
		}
	}
}

void print() {
	for (int i = order.size() - 1; i >= 0; --i) {
		cout << order[i] + 1;
		if (i != 0)
			cout << ' ';
	}
}

int main() {
	ios_base::sync_with_stdio(false);
	cin.tie(nullptr);
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	cin >> n >> m;

	// init graph
	used = new char[n]();
	graph.resize(n);

	// fill graph
	int u, v;
	for (int i = 0; i < m; ++i) {
		cin >> u >> v;
		graph[u - 1].push_back(v - 1);
	}

	dfs();
	print();
	delete[] used;
}