#include <iostream>
#include <vector>

using namespace std;

int n, m;
vector<vector<int>> graph;
bool *used;

void explore(int v) {
	used[v] = true;
	for (int i = 0; i < graph[v].size(); ++i) {
		if (!used[graph[v][i]])
			explore(graph[v][i]);
	}
}

int dfs() {
	int steps = 0;
	for (int i = 0; i < n; ++i) {
		if (!used[i]) {
			explore(i);
			++steps;
		}
	}
	return steps;
}

int main() {
	ios_base::sync_with_stdio(false);
	cin.tie(nullptr);
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	cin >> n >> m;

	// init graph
	used = new bool[n]();
	graph.resize(n);

	// fill graph
	for (int i = 0; i < m; ++i) {
		int u, v;
		cin >> u >> v;
		graph[u - 1].push_back(v - 1);
		graph[v - 1].push_back(u - 1);
	}

	cout << dfs();
	delete[] used;
}