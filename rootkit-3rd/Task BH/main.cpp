#include <iostream>
#include <vector>

using namespace std;

vector<vector<pair<int, int>>> graph;
int n, m,
	 *degrees;
char *used;
bool *visited;

void delete_arr() {
	delete[] used;
	delete[] degrees;
	delete[] visited;
}

bool degree() { // проверка степеней вершин графа
	for(auto edge : graph) {
		for(auto pair : edge) {
			++degrees[pair.second];
		}
	}

	for (int i = 0; i < n; ++i) {
		if (degrees[i] % 2 == 1)
			return false;
	}
	return true;
}

void explore(int v) {
	used[v] = true;
	for (int i = 0; i < graph[v].size(); ++i) {
		if (!used[graph[v][i].second])
			explore(graph[v][i].second);
	}
}

bool isConnected() { // проверка на связность графа
	int steps = 0;
	for (int i = 0; i < n; ++i) {
		if (!used[i]) {
			explore(i);
			++steps;
		}
		if (steps > 1) {
			return false;
		}
	}
	return true;
}

void euler(int u) {
	for (auto p : graph[u]) {
		int i = p.first, v = p.second;
		if (!visited[i]) {
			visited[i] = true;
			euler(v);
			cout << u + 1 << ' ';
		}
	}
}

void init() {
	for (int i = 0; i < m; ++i) {
		int u, v;
		cin >> u >> v;
		--u;
		--v;
		graph[u].push_back({ i, v });
		graph[v].push_back({ i, u });
	}
}

int main() {
	ios_base::sync_with_stdio(false);
	cin.tie(nullptr);
	// freopen("input.txt", "r", stdin);
	// freopen("output.txt", "w", stdout);

	cin >> n >> m;

	used = new char[n]();
	visited = new bool[m]();
	degrees = new int[n]();
	graph.resize(n);

	init();

	if (isConnected() && degree()) {
		cout << 1 << ' ';
		euler(0);
	} else
		cout << -1;
	delete_arr();
}