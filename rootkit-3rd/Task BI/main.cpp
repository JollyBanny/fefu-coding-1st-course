#include <iostream>
#include <vector>

using namespace std;

int n, m;
vector<vector<int>> G;
// const int INF = 1e9;

enum State {
	INF = 1000000000,
	NONE = -1,
};

int SST() {
	vector<bool> used(n);
	vector<int> min_way(n, INF), neighbore(n, -1);
	int min_tree = 0;
	min_way[0] = 0;
	for (int i = 0; i < n; ++i) {
		int nearest = NONE;
		for (int j = 0; j < n; ++j)
			if (!used[j] &&
				(nearest == NONE || min_way[j] < min_way[nearest])) {
				nearest = j;
			}

		if (min_way[nearest] == INF) {
			cout << -1;
			exit(0);
		}

		used[nearest] = true;
		if (neighbore[nearest] != NONE) {
			min_tree += G[nearest][neighbore[nearest]];
		}
		for (int to = 0; to < n; ++to)
			if (G[nearest][to] < min_way[to]) {
				min_way[to] = G[nearest][to];
				neighbore[to] = nearest;
			}
	}
	return min_tree;
}

int main() {
	ios_base::sync_with_stdio(false);
	cin.tie(nullptr);
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	cin >> n >> m;

	G.resize(n);
	for (int i = 0; i < n; ++i) {
		G[i].resize(n, INF);
	}

	for (int i = 0; i < m; ++i) {
		int u, v, w;
		cin >> u >> v >> w;
		--u, --v;
		G[u][v] = w;
		G[v][u] = w;
	}

	cout << SST();
	return 0;
}