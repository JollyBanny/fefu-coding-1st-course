#include <cmath>
#include <iostream>
#include <vector>

using namespace std;

const int N = 1000;
const int M = pow(N, 2);
int n, m;
bool graph[N][N];

// init matrix
void init_matrix() {
	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < N; ++j) {
			graph[i][j] = false;
		}
	}
}

// format edges
void input_edges() {
	int u, v;
	cin >> n >> m;
	for (int i = 0; i < m; ++i) {
		cin >> u >> v;
		graph[u - 1][v - 1] = true;
	}
}

void output_edges() {
	cout << n << ' ';
	int edges = 0;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			if (graph[i][j] == true)
				edges++;
		}
	}
	cout << edges << '\n';
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			if (graph[i][j] == true)
				cout << i + 1 << ' ' << j + 1 << '\n';
		}
	}
}

// format lists
void input_lists() {
	int vertices, value;
	cin >> n;
	for (int i = 0; i < n; ++i) {
		cin >> vertices;
		for (int j = 0; j < vertices; ++j) {
			cin >> value;
			graph[i][value - 1] = true;
		}
	}
}

void output_lists() {
	cout << n << '\n';
	for (int i = 0; i < n; ++i) {
		int count_v = 0;
		for (int j = 0; j < n; ++j) {
			if (graph[i][j] == true)
				count_v++;
		}
		cout << count_v << ' ';
		for (int k = 0; k < n; k++) {
			if (graph[i][k] == true)
				cout << k + 1 << ' ';
		}
		cout << '\n';
	}
}

// format mat
void input_mat() {
	cin >> n;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			cin >> graph[i][j];
		}
	}
}

void output_mat() {
	cin >> n;
	cout << n << '\n';
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			cout << graph[i][j] << ' ';
		}
		cout << '\n';
	}
}

int main() {
	ios_base::sync_with_stdio(false);
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	init_matrix();
	string format_in, format_out;
	cin >> format_in >> format_out;
	// input
	if (format_in == "edges") {
		input_edges();
	} else if (format_in == "lists") {
		input_lists();
	} else if (format_in == "mat") {
		input_mat();
	}
	// output
	if (format_out == "edges") {
		output_edges();
	} else if (format_out == "lists") {
		output_lists();
	} else if (format_out == "mat") {
		output_mat();
	}

	return 0;
}
