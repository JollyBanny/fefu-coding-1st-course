#include <iostream>
#include <vector>

using namespace std;
const int INF = 1000 * 1000;

struct V {
	int to, len;
};
vector<vector<V>> G;
vector<pair<int, int>> heap;

void print(vector<int> a) {
	for (int i = 0; i < a.size(); ++i) {
		if (a[i] >= INF) {
			cout << -1;
		} else {
			cout << a[i];
		}
		cout << " ";
	}
}

void heap_up(int id) {
	while (heap[id].second < heap[(id - 1) / 2].second) {
		swap(heap[id], heap[(id - 1) / 2]);
		id = (id - 1) / 2;
	}
}

void heap_down(int id) {
	int left, right;
	while (2 * id + 1 < heap.size()) {
		left = 2 * id + 1;
		right = 2 * id + 2;
		int j = left;
		if (right < heap.size() && heap[right].second < heap[left].second)
			j = right;
		if (heap[id].second <= heap[j].second)
			break;
		swap(heap[id], heap[j]);
		id = j;
	}
}

void heap_add(pair<int, int> pair) {
	heap.push_back(pair);
	heap_up(heap.size() - 1);
}

pair<int, int> heap_del() {
	pair<int, int> min = heap[0];
	swap(heap[0], heap[heap.size() - 1]);
	heap.pop_back();
	heap_down(0);
	return min;
}

vector<int> dijkstra(int s) {
	vector<bool> visited;
	vector<int> d;
	visited.resize(G.size(), false);
	d.resize(G.size(), INF);
	d[s] = 0;
	while (heap.size() != 0) {
		pair<int, int> root = heap_del();
		for (int j = 0; j < G[root.first].size(); ++j) {
			if (d[G[root.first][j].to] > d[root.first] + G[root.first][j].len) {
				d[G[root.first][j].to] = d[root.first] + G[root.first][j].len;
				heap_add({ G[root.first][j].to, d[G[root.first][j].to] });
			}
		}
	}
	return d;
}

int main() {
	ios_base::sync_with_stdio(false);
	cin.tie(nullptr);
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	int n, m, s;
	cin >> n >> m >> s;
	--s;
	G.resize(n);
	for (int i = 0; i < m; ++i) {
		int u, v, w;
		cin >> u >> v >> w;
		--u, --v;
		G[u].push_back(V{ v, w });
	}
	heap_add({ s, 0 });
	print(dijkstra(s));
	return 0;
}