#include <iostream>
#include <vector>

using namespace std;
const int INF = 1000000000;

int n, m;
vector<vector<int>> d;

void print(vector<vector<int>> a) {
	for (int i = 0; i < a.size(); ++i) {
		for (int j = 0; j < a[i].size(); ++j) {
			if (a[i][j] >= INF) {
				cout << ' ';
			} else {
				cout << a[i][j];
			}
			cout << " ";
		}
		cout << '\n';
	}
}

vector<vector<int>> FW() {
	for (int k = 0; k < n; ++k)
		for (int i = 0; i < n; ++i)
			for (int j = 0; j < n; ++j)
				if (d[i][k] < INF && d[k][j] < INF)
					d[i][j] = min(d[i][j], d[i][k] + d[k][j]);
	return d;
}

int main() {
	ios_base::sync_with_stdio(false);
	cin.tie(nullptr);
	// freopen("input.txt", "r", stdin);
	// freopen("output.txt", "w", stdout);
	cin >> n >> m;
	d.resize(n);

	for (int i = 0; i < n; ++i) {
		d[i].resize(n, INF);
		d[i][i] = 0;
	}

	for (int k = 0; k < m; ++k) {
		int u, v, w;
		cin >> u >> v >> w;
		--u;
		--v;
		d[u][v] = w;
	}
	print(FW());
	return 0;
}