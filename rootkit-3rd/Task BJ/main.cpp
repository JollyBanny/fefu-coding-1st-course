#include <iostream>
#include <vector>

using namespace std;

int n, m;
struct V {
	int to, len;
};
vector<vector<V>> G;
vector<pair<int, int>> heap;

enum State {
	INF = 1000000000,
};

void heap_up(int id) {
	while (heap[id].second < heap[(id - 1) / 2].second) {
		swap(heap[id], heap[(id - 1) / 2]);
		id = (id - 1) / 2;
	}
}

void heap_down(int id) {
	int left, right;
	while (2 * id + 1 < heap.size()) {
		left = 2 * id + 1;
		right = 2 * id + 2;
		int j = left;
		if (right < heap.size() && heap[right].second < heap[left].second)
			j = right;
		if (heap[id].second <= heap[j].second)
			break;
		swap(heap[id], heap[j]);
		id = j;
	}
}

void heap_add(pair<int, int> pair) {
	heap.push_back(pair);
	heap_up(heap.size() - 1);
}

pair<int, int> heap_del() {
	pair<int, int> min = heap[0];
	swap(heap[0], heap[heap.size() - 1]);
	heap.pop_back();
	heap_down(0);
	return min;
}

void explore(int v, vector<bool> &used) {
	used[v] = true;
	for (int i = 0; i < G[v].size(); ++i) {
		if (!used[G[v][i].to])
			explore(G[v][i].to, used);
	}
}

bool isConnected() {
	vector<bool> used(n);
	explore(0, used);
	for (int i = 0; i < n; ++i) {
		if (used[i] == false)
			return false;
	}
	return true;
}

int SST() {
	vector<bool> visited(n);
	vector<int> min_way(n, INF);
	int min_tree = 0;
	min_way[0] = 0;
	while (heap.size() != 0) {
		pair<int, int> root = heap_del();

		if (visited[root.first])
			continue;
		visited[root.first] = true;
		min_tree += root.second;

		for (auto edge : G[root.first]) {
			if (!visited[edge.to]) {
				heap_add({ edge.to, edge.len });
			}
		}
	}
	return min_tree;
}

int main() {
	ios_base::sync_with_stdio(false);
	cin.tie(nullptr);
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	cin >> n >> m;

	G.resize(n);

	for (int i = 0; i < m; ++i) {
		int u, v, w;
		cin >> u >> v >> w;
		--u, --v;
		G[u].push_back({ v, w });
		G[v].push_back({ u, w });
	}
	if (isConnected()) {
		heap_add({ 0, 0 });
		cout << SST();
	} else {
		cout << -1;
		return 0;
	}
	return 0;
}