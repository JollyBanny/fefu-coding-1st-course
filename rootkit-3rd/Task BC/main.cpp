#include <iostream>
#include <vector>

using namespace std;
const int INF = 1000 * 1000;
const int NONE = -1;

struct V {
	int to, len;
};
vector<vector<V>> G;

void print(vector<int> a) {
	for (int i = 0; i < a.size(); ++i) {
		if (a[i] >= INF) {
			cout << -1;
		} else {
			cout << a[i];
		}
		cout << " ";
	}
}

vector<int> dijkstra(int s) {
	vector<bool> visited;
	vector<int> d;
	visited.resize(G.size(), false);
	d.resize(G.size(), INF);
	d[s] = 0;
	for (int i = 0; i < G.size(); ++i) {
		int nearest = NONE;

		for (int j = 0; j < G.size(); ++j) {
			if (!visited[j] && (nearest == NONE || d[j] < d[nearest])) {
				nearest = j;
			}
		}
		if (d[nearest] == INF)
			break;

		visited[nearest] = true;

		for (int j = 0; j < G[nearest].size(); ++j) {
			if (d[G[nearest][j].to] > d[nearest] + G[nearest][j].len) {
				d[G[nearest][j].to] = d[nearest] + G[nearest][j].len;
			}
		}
	}
	return d;
}

int main() {
	ios_base::sync_with_stdio(false);
	cin.tie(nullptr);
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	int n, m, s;
	cin >> n >> m >> s;
	--s;
	G.resize(n);
	for (int i = 0; i < m; ++i) {
		int u, v, w;
		cin >> u >> v >> w;
		--u, --v;
		G[u].push_back(V{ v, w });
	}
	print(dijkstra(s));
	return 0;
}