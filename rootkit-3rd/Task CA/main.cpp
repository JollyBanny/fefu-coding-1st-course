#include <iostream>
#include <vector>

using namespace std;
typedef long long ll;

vector<int> color;
vector<vector<int>> graph;

int timer;
vector<bool> used;
vector<int> tin, tup, childs, bridge;

void dfs_child(int v) {
	childs[v] = 1;
	for (int to : graph[v]) {
		if (childs[to] == 0) {
			dfs_child(to);
			childs[v] += childs[to];
		}
	}
}

void dfs_bridges(int v, int p = -1) {
	used[v] = true;
	tin[v] = tup[v] = timer++;
	for (int to : graph[v]) {
		if (to == p)
			continue;
		if (used[to])
			tup[v] = min(tup[v], tin[to]);
		else {
			dfs_bridges(to, v);
			tup[v] = min(tup[v], tup[to]);
			if (tup[to] > tin[v])
				bridge.push_back(childs[to]);
		}
	}
}

void find_bridges() {
	timer = 0;
	for (int i = 0; i < graph.size(); ++i)
		if (!used[i])
			dfs_bridges(i);
}

int components = 0;
void explore(int v) {
	color[v] = components + 1;
	for (int to : graph[v]) {
		if (!color[to])
			explore(to);
	}
}

void dfs() {
	for (int i = 0; i < graph.size(); ++i) {
		if (!color[i]) {
			explore(i);
			dfs_child(i);
			++components;
		}
	}
}

int main() {
	ios_base::sync_with_stdio(false);
	cin.tie(nullptr);
    cout.tie(nullptr);

	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	int n, m;
	cin >> n >> m;

	graph.resize(n);
	for (int i = 0; i < m; ++i) {
		int u, v;
		cin >> u >> v;
		graph[u - 1].push_back(v - 1);
		graph[v - 1].push_back(u - 1);
	}

	color.resize(n);
	childs.resize(n);
	dfs();

	used.resize(n);
	tin.resize(n);
	tup.resize(n);
	find_bridges();

	if (components > 2)
		cout << 0;
	else if (components == 2) {
		int cmp1 = 0, cmp2 = 0;
		for (int i : color) {
			if (i == 1)
				++cmp1;
			else
				++cmp2;
		}
		cout << (ll)cmp1 * (ll)cmp2 * (ll)(m - bridge.size());
	} else if (components == 1) {
		ll ways = (ll)((m - bridge.size()) * ((ll)n * (n - 1) / 2 - m));
		for (int tmp : bridge)
			ways += (ll)((n - tmp) * (ll)tmp - 1);
		cout << ways;
	}
	return 0;
}