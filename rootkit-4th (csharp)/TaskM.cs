using System;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections.Generic;

namespace myApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Console.InputEncoding = Encoding.UTF8;
            Console.OutputEncoding = Encoding.UTF8;
            var n = Convert.ToInt32(Console.ReadLine());
            List<string> names = new List<string>();
            for (int k = 0; k < n; ++k) {
                names.Add((Console.ReadLine()));
            }
            names.Sort();
            Console.Write(names[0]);
       }
    }
}