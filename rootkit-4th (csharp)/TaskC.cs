using System;
using System.Text;

namespace myApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            string text = Console.ReadLine();
            string ans = "";

            if (text != null && (text.Contains(','))) {
                ans += "сложное ";
            }
            if (text != null && (text.Contains('!')) && (text.Contains('?'))) {
                ans += "вопросительно-восклицательное";
            }
            else if (text != null && (text.Contains('!'))) {
                ans += "восклицательное";
            }
            else if(text != null && (text.Contains('?'))) {
                ans += "вопросительное";
            }
            else ans += "повествовательное";

            Console.OutputEncoding = Encoding.GetEncoding("windows-1251");
            Console.WriteLine(ans);
        }
    }
}
