using System;
using System.Text;
using System.Collections.Generic;

namespace myApp
{
    public class Comparer : IComparer<string>
    {
        public int Compare(string x, string y)
        {
            return x[1].CompareTo(y[1]);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var input = Console.ReadLine().Split(' ');
            List<string> words = new List<string>();
            foreach (string i in input) {
                words.Add(i);
            }
            words.Sort(new Comparer());

            for (int i = 0; i < words.Count; i++) {
                Console.Write(words[i]);
                if (i < words.Count - 1) Console.Write(" ");
            }
        }
    }
}