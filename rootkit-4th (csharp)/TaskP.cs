﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MyCollection
{
    public interface IStack<T> : IEnumerable, IEnumerable<T>
    {
        // Сводка:
        //     Gets the number of elements contained in the IStack.
        //
        // Возврат:
        //     The number of elements contained in the IStack.
        public int Count { get; }
        //
        // Сводка:
        //     Removes all objects from the IStack.
        public void Clear();
        //
        // Сводка:
        //     Determines whether an element is in the IStack.
        //
        // Параметры:
        //   item:
        //     The object to locate in the IStack. The value can
        //     be null for reference types.
        //
        // Возврат:
        //     true if item is found in the IStack; otherwise, false.
        public bool Contains(T item);
        //
        // Сводка:
        //     Returns the object at the top of the IStack without
        //     removing it.
        //
        // Возврат:
        //     The object at the top of the IStack.
        //
        // Исключения:
        //   T:System.InvalidOperationException:
        //     The IStack is empty.
        public T Peek();
        //
        // Сводка:
        //     Removes and returns the object at the top of the IStack.
        //
        // Возврат:
        //     The object removed from the top of the IStack.
        //
        // Исключения:
        //   T:System.InvalidOperationException:
        //     The IStack is empty.
        public T Pop();
        //
        // Сводка:
        //     Inserts an object at the top of the IStack.
        //
        // Параметры:
        //   item:
        //     The object to push onto the IStack. The value can
        //     be null for reference types.
        public void Push(T item);
        //
        // Сводка:
        //     Copies the IStack to a new array.
        //
        // Возврат:
        //     A new array containing copies of the elements of the IStack.
        public T[] ToArray();
    }
    class MStack<T> : IStack<T>
    {
        private T[] _stack = new T[10];
        private int _count = 0;

        public int Count {
            get {
                return _count;
            }
        }

        public void Clear()
        {
            _stack = new T[10];
            _count = 0;
        }

        public bool Contains(T item)
        {
            for (int i = 0; i < _count; ++i) {
                if (_stack[i].Equals(item))
                    return true;
            }
            return false;
        }

        public bool isEmpty()
        {
            if (Count == 0)
                return true;
            return false;
        }

        public T Peek()
        {
            if(isEmpty())
                throw new InvalidOperationException("The IStack is empty");
            return _stack[Count - 1];
        }

        public T Pop()
        {
            if(isEmpty())
                throw new InvalidOperationException("The IStack is empty");
            var item = Peek();
            --_count;
            return item;
        }

        public void Push(T item)
        {
            if (_stack.Length == Count) {
                Array.Resize(ref _stack, _stack.Length * 2);
            }
            _stack[Count] = item;
            ++_count;
        }

        public T[] ToArray()
        {
            T[] _newStack = new T[_count];
            Array.Copy(_stack, _newStack, _count);
            for (int i = 0; i < Count; ++i) {
                _newStack[i] = _stack[Count - 1 - i];
            }
            return _newStack;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new StackEnum(_stack, _count);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new StackEnum(_stack, _count);
        }
        class StackEnum : IEnumerator, IEnumerator<T>
        {
            private T[] _stack;

            int position;

            public StackEnum(T[] list, int _count)
            {
                _stack = list;
                position = _count;
            }

            public bool MoveNext()
            {
                position--;
                return (position >= 0);
            }

            public void Reset()
            {
                position = _stack.Length;
            }

            public void Dispose()
            {

            }

            object IEnumerator.Current
            {
                get
                {
                    return Current;
                }
            }

            public T Current
            {
                get
                {
                    try
                    {
                        return _stack[position];
                    }
                    catch (IndexOutOfRangeException)
                    {
                        throw new InvalidOperationException();
                    }
                }
            }

            T IEnumerator<T>.Current
            {
                get
                {
                    try
                    {
                        return _stack[position];
                    }
                    catch (IndexOutOfRangeException)
                    {
                        throw new InvalidOperationException();
                    }
                }
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var stackFixed = new MStack<string>();

            stackFixed.Push("Lora");
            stackFixed.Push("Dale");
            stackFixed.Push("Jack");
            stackFixed.Push("Andrew");
            stackFixed.Push("Linda");
            stackFixed.Push("Lisa");

            foreach (var item in stackFixed) {
                Console.Write($"{item} ");
            }
        }
    }
}