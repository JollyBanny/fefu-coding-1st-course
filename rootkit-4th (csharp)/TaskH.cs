using System;
using System.Text;
using System.Collections.Generic;

namespace myApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Console.OutputEncoding = Encoding.GetEncoding("windows-1251");
            var input = Console.ReadLine().Split(' ');
            string first = "";
            // int k = 0;
            bool check = true;

            foreach (string i in input) {
                first += i.ToLower();
            }
            for (int i = first.Length - 1, k = 0; i >= 0 && k < first.Length; --i, k++) {
                if (first[i] != first[k]) check = false;
            }
            if (check) Console.Write("True");
            else Console.Write("False");
        }
    }
}