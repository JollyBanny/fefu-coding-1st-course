using System;
using System.Text;
using System.Collections.Generic;

namespace myApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Console.OutputEncoding = Encoding.GetEncoding("windows-1251");
            var input = Console.ReadLine().Split(' ');
            int b = 0;

            foreach (string i in input) {
                if (!i.Contains(",") && !i.Contains(".") && !i.Contains("?") && !i.Contains("!")) {
                    b++;
                }
            }
            Console.Write(b);
        }
    }
}