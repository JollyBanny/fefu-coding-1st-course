using System;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections.Generic;

namespace myApp
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = new StreamReader("input.txt");
            StreamWriter writer = new StreamWriter("output.txt");

            var input = reader.ReadLine();
            bool check = false;
            for (int i = 0; i < input.Length; ++i) {
                if (input[i] >= 'A' && input[i] <= 'Z') {
                    check = true;
                    break;
                }
            }
            if (check) {
                for (int i = 0; i < input.Length; ++i) {
                    if(input[i] >= 'A' && input[i] <= 'Z') {
                        if(i != 0) {
                            writer.Write($"_{char.ToLower(input[i])}");
                        }
                        else {
                            writer.Write($"{char.ToLower(input[i])}");
                        }
                    }
                    else
                        if (i < input.Length)
                            writer.Write(input[i]);
                }
            }
            else {
                var words = input.Split('_');
                for (int i = 0; i < words.Length; ++i) {
                    for (int j = 0; j < words[i].Length; ++j) {
                        if(j == 0) {
                            writer.Write(char.ToUpper(words[i][j]));
                        }
                        else {
                            writer.Write(words[i][j]);
                        }
                    }
                }
            }
            writer.Flush();
        }
    }
}