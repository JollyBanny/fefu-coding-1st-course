using System;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections.Generic;

namespace myApp
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = new StreamReader("input.txt");
            StreamWriter writer = new StreamWriter("output.txt");

            var map = new Dictionary<string, List<string>>();
            var n = Convert.ToInt32(reader.ReadLine());
            for (int i = 0; i < n; ++i) {
                var input = reader.ReadLine().Split('	');
                List<string> pupils = new List<string>();
                var key = input[0];
                var name = input[1];

                if (!map.ContainsKey(key)) {
                    pupils.Add(name);
                    map.Add(key, pupils);
                }
                else {
                    map.TryGetValue(key, out pupils);
                    pupils.Add(name);
                    map[key] = pupils;
                }
            }
            foreach (KeyValuePair<string, List<string>> kvp in map.OrderBy(x => x.Key)) {
                writer.WriteLine(kvp.Key);
                kvp.Value.Sort();
                foreach (string arr in kvp.Value) {
                    writer.WriteLine(arr);
                }
            }
            writer.Flush();
        }
    }
}