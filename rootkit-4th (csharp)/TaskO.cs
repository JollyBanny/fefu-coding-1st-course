﻿using System;
using System.Reflection;
using System.Collections.Generic;

namespace MyClass
{
    class MethodsInfo
    {
        private static Type getType(MethodInfo method)
        {
            return method.ReturnType;
        }
        private static string getName(MethodInfo method)
        {
            return method.Name;
        }
        private static List<string> getParams(MethodInfo method)
        {
            var listParameters = new List<string>();
            var parameters = method.GetParameters();
            foreach (var param in parameters)
            {
                listParameters.Add($"{param.ParameterType} {param.Name}");
            }
            return listParameters;
        }
        public static List<string> methodInfo(MethodInfo[] methods)
        {
            var info = new List<string>();
            foreach (var method in methods)
            {
                var paramList = getParams(method);
                var fullParams = "(";
                for (int i = 0; i < paramList.Count; ++i)
                {
                    fullParams += paramList[i];
                    if (i < paramList.Count - 1)
                    {
                        fullParams += ", ";
                    }
                }
                fullParams += ")";
                var _method = getType(method) + " " + getName(method) + " " + fullParams + ";";
                info.Add(_method);
            }
            return info;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var _class = Console.ReadLine();
            var methods = Type.GetType(_class).GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static);
            var listMethods = MethodsInfo.methodInfo(methods);
            listMethods.Sort();
            foreach (var method in listMethods)
            {
                Console.WriteLine(method);
            }
        }
    }
}