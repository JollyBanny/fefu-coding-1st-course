using System;
using System.Text;
using System.Collections.Generic;

namespace myApp
{
    class Program
    {
        static int gcd(int x, int y) {
            int low = 0;
            if (x == y) return x;
            else if (x > y) {
                low = y;
                while(x % low != 0 || y % low != 0) {
                    low--;
                }
            }
            else {
                low = x;
                while(x % low != 0 || y % low != 0) {
                    low--;
                }
            }
            return low;
        }
        static void Main(string[] args)
        {
            var input = Console.ReadLine().Split(' ');
            int a = Convert.ToInt32(input[0]), b = Convert.ToInt32(input[1]), c = Convert.ToInt32(input[2]);
            Console.Write($"{gcd(a, b)} {gcd(a, c)} {gcd(b, c)}");
        }
    }
}