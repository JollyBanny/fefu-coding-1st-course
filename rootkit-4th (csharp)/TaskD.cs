using System;
using System.Text;

namespace myApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Console.OutputEncoding = Encoding.GetEncoding("windows-1251");
            int a = Convert.ToInt32(Console.ReadLine());

            if (a % 3 == 0 && a % 5 == 0) {
                Console.WriteLine("FizzBuzz");
            }
            else if (a % 3 == 0) {
                Console.WriteLine("Fizz");
            }
            else if (a % 5 == 0) {
                Console.WriteLine("Buzz");
            }
            else {
                Console.WriteLine("");
            }
        }
    }
}