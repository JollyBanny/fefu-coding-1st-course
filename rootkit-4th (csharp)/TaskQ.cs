using System;
using System.Runtime.Serialization;

namespace MyExtensions
{
    internal class BadSizeException : Exception
    {
        public BadSizeException()
        {
        }

        public BadSizeException(string message) : base(message)
        {
        }

        public BadSizeException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected BadSizeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
    static class ArrayExtension
    {
        private static int checkCapacity(ref int[] dimensions)
        {
            var capacity = 1;
            for (int i = 0; i < dimensions.Length; ++i)
            {
                capacity *= dimensions[i];
            }
            return capacity;
        }

        public static Array Reshape(this Array array, params int[] dimensions)
        {
            if (checkCapacity(ref dimensions) < array.Length)
                throw new BadSizeException();

            var _array = Array.CreateInstance(typeof(object), dimensions);

            var dimLen = dimensions.Length;

            var indexes = new int[dimLen];
            while (indexes[0] < dimensions[0])
            {
                foreach (var item in array)
                {
                    _array.SetValue(item, indexes);

                    if (indexes[dimLen - i] < dimensions[dimLen - i] - 1)
                    {
                        ++indexes[dimLen - i];
                    }
                    else
                    {
                        while (indexes[dimLen - i] >= dimensions[dimLen - i] - 1 && i < dimLen)
                        {
                            indexes[dimLen - i] = 0;
                            ++i;
                            ++indexes[dimLen - i];
                        }
                        var i = 1;
                    }
                }
            }
            return _array;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var arr = new int[2, 3];
            arr[0, 0] = 1;
            arr[0, 1] = 2;
            arr[0, 2] = 3;
            arr[1, 0] = 4;
            arr[1, 1] = 5;
            arr[1, 2] = 6;

            var _arr = ArrayExtension.Reshape(arr, 1, 6);
            foreach (var item in _arr)
            {
                Console.WriteLine(item);
            }
        }
    }
}