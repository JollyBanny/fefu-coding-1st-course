using System;
using System.Text;
using System.Collections.Generic;

namespace myApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Console.OutputEncoding = Encoding.GetEncoding("windows-1251");
            var input = Console.ReadLine().Split(' ');
            var n = Convert.ToInt32(input[0]);
            var k = Convert.ToInt32(input[1]);

            input = Console.ReadLine().Split(' ');
            for (int i = 0; i < input.Length; ++i) {
                var index = (i + k) % n;
                Console.Write($"{input[index % n]} ");
            }
        }
    }
}