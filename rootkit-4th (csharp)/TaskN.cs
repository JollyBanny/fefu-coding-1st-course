using System;

namespace Fefu
{
    public class WrongDataException : Exception
    {
        public WrongDataException()
        {
        }

        public WrongDataException(string message) : base(message)
        {
        }

        public WrongDataException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }

    public interface IStudent
    {
        public int Age { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
    class Student : IStudent
    {
        private int _age;
        private string _firstName;
        private string _lastName;
        private bool CheckName(string value) {
            if (value[0] < 'A' || value[0] > 'Z' || value.Length == 0) {
                return false;
            }
            for (var i = 1; i < value.Length; ++i)
            {
                var symbol = value[i];
                if(symbol < 'a' || symbol > 'z') {
                    return false;
                }
            }
            return true;
        }
        public int Age {
            get {
                return _age;
            }
            set {
                if (value <= 0) {
                    throw new WrongDataException();
                }
                _age = value;
            }
        }
        public string FirstName {
            get {
                return _firstName;
            }
            set {
                if (!CheckName(value)) {
                    throw new WrongDataException();
                }
                _firstName = value;
            }
        }
        public string LastName {
            get {
                return _lastName;
            }
            set {
                if (!CheckName(value)) {
                    throw new WrongDataException();
                }
                _lastName = value;
            }
        }
        public Student(string firstName, string lastName, int age) {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Age = age;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var input = Console.ReadLine().Split(' ');
            var name = input[0];
            var lastName = input[1];
            var age = Convert.ToInt32(input[2]);
            var student = new Student(name, lastName, age);
        }
    }
}