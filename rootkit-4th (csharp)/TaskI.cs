using System;
using System.Text;

namespace myApp
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Console.OutputEncoding = Encoding.GetEncoding("windows-1251");
            var input = Console.ReadLine().Split(' ');
            double sum = 0;
            foreach (string i in input) {
                sum += Math.Pow((Convert.ToDouble(i)), 2);
            }
            Console.WriteLine(Math.Sqrt(sum));
        }
    }
}